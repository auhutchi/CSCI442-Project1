/**
 * Contains the implementation of functions declared in command.h.
 */

#include "command.h"
#include <algorithm>
#include <iostream>
#include <iterator>
#include <string.h>

using namespace std;

// This function partitions multiple comands into a vector of command structs
bool partition_tokens(vector<string> tokens, vector<command_t>& commands) {

	// Initialize alot of stuff 
	size_t inputSize = tokens.size();	// Number of total tokens
	size_t currentToken = 0;		// Current token number to analyze
	bool commandFull;			// Used to deterimine when to push command and start a new one
	bool pipeBefore = false;		// Used for input_type and error checking
	bool pipeAfter = false;			// Used for output_type
	bool inFileNext = false;		// Used to deterimine if the argument at hand is a file name
	bool outFileNext = false;		// Used to determine if the argument is a file name
	bool fileAppend = false;		// Used to determine output_type
	bool fileError = false;			// Used to check to see if no file name was given (error checking)
	bool missingCommand = false;		// Used to see if no command was given (error checkign)
	string fileErrMessage;			// The output messafe for file errors
	string cmdErrMessage = "should not allow empty command";	// The output messagfe for file errors
	int numInputs = 0;			// Used for error checkign, to see if too many inputs
	int numOutputs = 0;			// Used for error checking to see if too many oyutouyts				

	// Start decoding the tokens into command structs
	while (currentToken < inputSize){				// As long as there's tokens, keep rolling
		
		// Initialize a command struct
		commandFull = false;		
		missingCommand = true;
		command_t commandTemp;
		
		// Pull info from tokens and populate command struct
		while (!commandFull){
			if (currentToken >= inputSize) {		// If no more tokens, then populate. 
				commandFull = true;
				continue;
			} else {					// Else collect population info
				string arg = tokens[currentToken];	// Get a token
				currentToken++;				// Increment counter token for next iteration
				if (arg == "|") {			// If pipe, then enough info to populate
					commandFull = true;
					pipeAfter = true;			// Used for next iteration and error checking
					numOutputs++;				// Used for error checking (too many outputs)
					continue;
				} else if (arg == "<") {		// If file input symbol,
					inFileNext = true;			// Tell decoder there's going to be a file next iteration
					fileError = true;			// Flag that there's currently no file name associated
					fileErrMessage = "missing file argument for '<'";
					cmdErrMessage = "missing command for '<'";
					numInputs++;				// Increase input count, for error checking
					continue;
				} else if (arg == ">") {		// If file output symbol
					outFileNext = true;			// Tell decoder there's going to be a file next iteration
					fileError = true;			// Flag that there's current no file name associated
					fileErrMessage = "missing file argument for '>'";
					cmdErrMessage = "missing command for '>'";
					numOutputs++;				// Increase output count for error checkign
					continue;
				} else if (arg == ">>") {		// If file append symbol
					outFileNext = true;			// Tell decoder there's going to be a file next iteration
					fileAppend = true;			// Tell decoder that we're appending to a file
					fileError = true;			// Flag that there's current;y no file associated
					fileErrMessage = "missing file argument for '>>'";
					cmdErrMessage = "missing command for '>>'";
					numOutputs++;				// Increase output count for error checkign 
					continue;
				} else if (inFileNext == true) {	// If looking for a input file name
					commandTemp.infile = arg;		// Save token as the infile name
					fileError = false;			// take down flag telling no file name given
					continue;
				} else if (outFileNext == true) {	// If looking for a output file name
					commandTemp.outfile = arg;		// Save token as the outfile name
					fileError = false;			// Take down flag telling no file name given
					continue;
				} else {				// If no special symbols
					missingCommand = false;			// Unflag missing command
					commandTemp.argv.push_back(arg);	// Push command or argumet onto command struct arguments
				}
			}		 
		}

		// Populate input/output types
		if (pipeBefore == true) {				// If there was a pipe before this command
			commandTemp.input_type = READ_FROM_PIPE;		//Set input_type to read from the pipe
			commandTemp.piping = true;	
			numInputs++;						// Increase the input count for error checking
		} else if (inFileNext == true) {			// If reading a file
			commandTemp.input_type = READ_FROM_FILE;		// Set input type to read from the file
			commandTemp.fileRedirection = true;
		}
		if (pipeAfter == true) {				// If there's a pipe after this command, 
			commandTemp.output_type = WRITE_TO_PIPE;		// Set output to write to pipe
			commandTemp.piping = true;	
			pipeBefore = true;
		} else if (outFileNext == true) {			// If there was an output file
			commandTemp.fileRedirection = true;
			if (fileAppend == true) {				// And wanted to append to it
				commandTemp.output_type = APPEND_TO_FILE;		// Set output type to append
			} else {	
				commandTemp.output_type = WRITE_TO_FILE;	// else, just set outpute type to write
			}
		}

		


		// Check for and print errors.
		if (fileError == true) {				// If there was a file erro
			cout << fileErrMessage;					// Print error
			return false;						// and exit
		}
		if (missingCommand) {					// If a command was never given
			cout << cmdErrMessage;					// Print error
			return false;						// and exit
		}
		if (numInputs > 1) {					// If there was more than one input
			cout << "command has multiple inputs";			// Write error
			return false;						// and exit
		} 
		if (numOutputs > 1) {					// If there was more than one output
			cout << "command has multiple outputs";			// write error
			return false;						// and exit
		}

		
		// Reset stuff that needs to be reset for next command interations.
		if (pipeAfter == false){		// If there wasn't a piper after the command
		pipeBefore = false;				// Then reset pipeBefore so there's not an error
		}
		pipeAfter = false;
                inFileNext = false;
                outFileNext = false;
                fileAppend = false;
		numInputs = 0;
		numOutputs =0;
	
		// WOW, FINALLY, make the populated command offical
		commands.push_back(commandTemp);

	}
	
	// Check for empty ended pipes
	if (pipeBefore) {
		cout << "should not allow empty command";
		return false;
	}
	
	// YAY, function is complete
	return true;
}


// Feel free to ignore everything below here. It's just code to allow you to
// cout a command in a nice, pretty format. =)


const char* input_types[] = {
  "READ_FROM_STDIN",
  "READ_FROM_FILE",
  "READ_FROM_PIPE"
};


const char* output_types[] = {
  "WRITE_TO_STDOUT",
  "WRITE_TO_PIPE",
  "WRITE_TO_FILE",
  "APPEND_TO_FILE"
};


ostream& operator <<(ostream& out, const command_t& cmd) {
  copy(cmd.argv.begin(), cmd.argv.end(), ostream_iterator<string>(out, " "));

  out << "\n    input:   " << input_types[cmd.input_type]
      << "\n    output:  " << output_types[cmd.output_type]
      << "\n    infile:  " << cmd.infile
      << "\n    outfile: " << cmd.outfile;

  return out;
}
