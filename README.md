# Project 1

Your final submission must contain a README file with the following:
 * Your name.
	- Austin Hutchison

 * A list of all the files in your submission and what each does.
	- README.md
		-This is just a readme to communicate info about the shell
	- command.cpp / command.h
		- These files handle command partitioning
	- main.cpp
		- simply delegates to an instance of the Shell class
	- makefile
		- used to compile the shell
	- shell.h
		- This file contains the definition of the Shell class, which is responsible for all of the functionality of the shell
	- shell_builtins.cpp
		- This file contains the implementations of the builtin functions provided by the shell
	- shell_cmd_execution.cpp
		- This file contains implementations of the functions that are responsible for executing commands that are not builtins.
	- shell_core.cpp
		- Contains implementations of methods responsible for high-level behavior of the shell.
	- shell_tab_completion.cpp
		- This file contains implementations of the functions that provide tab-completion for the shell.

 * Any unusual / interesting features in your programs.

	- Nothing of note. I am not an over achiever when a project takes this long.

 * Approximate number of hours you spent on the project.

	- An ungodly amount of time. I might need to invest into a better desk chair.
