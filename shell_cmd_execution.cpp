/**
 * This file contains implementations of the functions that are responsible for
 * executing commands that are not builtins.
 *
 * Much of the code you write will probably be in this file. Add helper methods
 * as needed to keep your code clean. You'll lose points if you write a single
 * monolithic function!
 */

#include "shell.h"
#include <iostream>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include "command.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;


// Convert vector to char**
vector<char*>  stringVecToCharVec(vector<string>& tokens) {
	vector<char*> argz;
	for (unsigned int i=0; i < tokens.size(); i++){
		argz.push_back((char*) tokens[i].c_str());
	}
	argz.push_back(NULL);
	return argz;
}

// Open the input file and handle redirection
int inFileOpenAndRedirect(command_t& command) {
	if (command.input_type == READ_FROM_FILE) {
		int file = open(command.infile.c_str(), O_RDONLY, 0644);  // Open infile
		if (file < 0) {					// Check for opening errors
			cout << "Error opening read file" << endl;
			return file;
		}
		int dupErr = dup2(file, 0);			// Redirect standard in to infile
		if (dupErr < 0) {				// Check for redirection errors
			cout << "Error redirecting infile" << endl;
			return -1;
		}
		return file;
	} else {
		return -10;
	}
}

// Open the output file and handle redirection
int outFileOpenAndRedirect(command_t& command){
	int file;
	if (command.output_type == WRITE_TO_FILE) {
		file = open(command.outfile.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644);	// Open outfile
	} else if (command.output_type == APPEND_TO_FILE) {
		file = open(command.outfile.c_str(), O_WRONLY | O_CREAT | O_APPEND, 0644);	// Open outfile
	} else {
		return -10;
	}
	if (file < 0) {						// Check for opening erros
		cout << "Error opening write file" << endl;
		return file;
	}
	int dupErr = dup2(file, STDOUT_FILENO);				// Redirect standard out to outfile
	if (dupErr < 0) {
		cout << "ERROR redirecting outfile" << endl;
		return -1;
	}
	return file;
}

int payThePiper(command_t& command, int (&writePipe)[2], int &readPipe) {	// Pipe redirection
	if (command.input_type == READ_FROM_PIPE){
		int dupErr = dup2(readPipe, STDIN_FILENO);
		if (dupErr < 0) {
			cout << "ERROR redirecting read pipe" << endl;
			return -1;
		}
	} 
	if (command.output_type == WRITE_TO_PIPE){
		int dupErr = dup2(writePipe[1], STDOUT_FILENO);
		if (dupErr < 0) {
			cout << "ERROR redirecting read pipe" << endl;
			return -1;
		}
	} 
	return 0;
}


int Shell::execute_external_command(vector<string>& tokens) {

	// Get all commands from tokens
	vector<command_t> commands;
	bool partitioned = partition_tokens(tokens, commands);
	if (!partitioned) {
		cout << "Command partioning failed" << endl;
		return EXIT_FAILURE;
	}
	
	int mrPipe[2];				// Mr Pipe
	int seniorPipe;				// Old mrPipe
	int exitCode = 0;			// Child exit codes
	int returnCode = 0;

	for (unsigned int i=0; i < commands.size(); i++){		// Go through the commands
		
		// Get a command and determine what it wants.
		command_t command = commands[i];		// Get a command
		if (command.output_type == WRITE_TO_PIPE){
			// Create a pipe
			int pipeErr = pipe(mrPipe);
			if (pipeErr == -1) {
				perror("Pipe Failed ");
				return EXIT_FAILURE;
			}
		}
	
	
	
		int pid;
		if ((pid = fork()) == -1){			// Fork a new process
			perror("Failed to fork");
			return EXIT_FAILURE;
		}
		
		if (pid ==0){
			// Handle redirections
			int inFile, outFile;
			if (command.fileRedirection){
				inFile = inFileOpenAndRedirect(command);	// Open and redirect  infile if there is one
				if (inFile < 0) {				// Check for errors
					if (inFile == -10) {
						// Do nothing
					} else {
						return EXIT_FAILURE;
					}
				}
				outFile = outFileOpenAndRedirect(command); // Open and redirect outfile if there is one
				if (outFile < 0) {
					if (outFile == -10) {

						// Do nothing
					} else {
						return EXIT_FAILURE;
					}
				}
			}
			
						
			if (command.piping) {							// For some reason error checking is
				//Handle the pipes						// Causing lots of issues. I don't
												// Understand why. ????
		//		int pipeErr = payThePiper(command, mrPipe, seniorPipe);
		//		if (pipeErr < 0) {
		//			cout << "Pipe Redirection Err" << endl;
		//			return EXIT_FAILURE;
		//		}
				if (command.input_type == READ_FROM_PIPE) {
					dup2(seniorPipe, STDIN_FILENO);
			//		if (dupErr < 0) {
			//			cout << "ERROR redirecting read pipe" << endl;
			//			return EXIT_FAILURE;
			//		}
				}
				if (command.output_type == WRITE_TO_PIPE){
					dup2(mrPipe[1], STDOUT_FILENO);
			//		if (dupErr < 0) {
			//			cout << "ERROR redirecting  write pipe" << endl;
			//			return EXIT_FAILURE;
			//		}
				}
			}

			close(mrPipe[0]);	// Don't let Mr Pipe listen


			// Handle execution
			vector<char*> args = stringVecToCharVec(command.argv);
			int exErr = execvp(args[0], &args[0]);	// Execute external command
			if (exErr == -1) {
				perror("execvp error");
				return EXIT_FAILURE;
			}
						
		} else {
			
			//Close pipes
			close(mrPipe[1]);		//Don't let Mr Pipe talk
						
			waitpid(pid, &exitCode, 0);			// Wait for child
			if (WIFEXITED(exitCode)){
				returnCode =  WEXITSTATUS(exitCode);
			}
		
			//Close old pipe
			close(seniorPipe);
			
			//Age young pipe into old pipe
			seniorPipe = mrPipe[0];
		
		}	
	}
	return returnCode;
}
