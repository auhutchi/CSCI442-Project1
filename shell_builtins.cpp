/**
 * This file contains the implementations of the builtin functions provided by
 * the shell (or will, once you've finished implementing them all).
 */

#include "shell.h"
#include <iostream>
#include <stdlib.h>		// Just in case
#include <sys/types.h>		// Just in case
#include <dirent.h>		// Used in ls command
#include <unistd.h>		// Used in ls command
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <map>			// Used by alias command
#include <readline/history.h>	// Used by history

using namespace std;

// ls and ls <directory>: list the contents of the specified directory,
//                        or of the current working direc-tory when no argument is provided.
int Shell::com_ls(vector<string>& argv) {
  
	DIR *directory;			// Create a directory stream
	struct dirent *dirrecords;	// This will contain file names
	int errorsave;
	const long size = pathconf(".", _PC_PATH_MAX);
	char buf[(size_t)size];
	char *dirname;	                // Contains the directory name.

	if (argv.size() == 1) {				// IF no arguments given, get current working directory

		dirname = getcwd(buf, (size_t)size);	// Get the current working directory
		if (dirname == NULL) {			// If error reading cwd, output error and exit
			perror("getcwd error");
			return EXIT_FAILURE;
		}
	directory = opendir(dirname);			// Open a directory stream

	} else if (argv.size() == 2) {			// Else if directory given, load it into the directory name pointer
		string argvannoying = argv[1];
		directory = opendir(argvannoying.c_str());		// Open a directory stream

	} else {						// Return error if one or more arguments given
		cout << "Too many arguments, I'm not that intelligent!!!!" << endl;
		 return EXIT_FAILURE;			
	}
	
	
		if (directory == NULL) {		// If error while opening  directory, output error and exit
			cout << argv[1] << endl;
			perror("opendir error");
			return EXIT_FAILURE;
		}

	errorsave = errno;				// Save current errno to check against later
	while ((dirrecords = readdir(directory)) != NULL){
		printf ("%s\n", dirrecords->d_name);	// Print out everything in the specified directory
	}
	if (errorsave != errno) {			// If reading directory didn't work well, then print error.
		perror("readdir error");
		return EXIT_FAILURE;
	}

 	closedir(directory);				// Close the directory stream
	 return 0;					// exit the ls function
}


// cd and cd <directory>: change the current working directory to the given directory, 
//			  or to the user’s home directory when no argument is provided

int Shell::com_cd(vector<string>& argv) {
		
		int cderror = 66;
		const char* hommie = "HOME";
		if (argv.size() == 1) {			// Chane to home directory
			char *homedir = getenv(hommie);		// Get home directory
			cderror = chdir(homedir);
		} else if (argv.size() == 2){		// Change to user inputed directory
			cderror = chdir(argv[1].c_str());	
		} else {				// Return error if more than one arguments give. 
			cout << "Too many arguments, I'm not that intelligent!!!!" << endl;
		 	return EXIT_FAILURE;			
		}
		if (cderror == 0) {			// If directory change went well, return
			return 0;
		} else {				// Else if directory change failed, print and return error
			perror("chdir error");
			return EXIT_FAILURE;
		}
	}



// pwd: outputs the current working directory.
int Shell::com_pwd(vector<string>& argv) {
	const long size = pathconf(".", _PC_PATH_MAX);
	char buf[(size_t)size];
	char *dirname;	 		               	// Contains the directory name.

	if (argv.size() == 1) {				// IF no arguments given, get current working directory

		dirname = getcwd(buf, (size_t)size);	// Get the current working directory
		if (dirname == NULL) {			// If error reading cwd, output error and exit
			perror("getcwd error");
			return EXIT_FAILURE;
		}
	} else {					// Return error if one or more arguments given
		cout << "Too many arguments, I'm not that intelligent!!!!" << endl;
		 return EXIT_FAILURE;			
	}
	cout << dirname << endl;			// Cout the current directory name
	return 0;
}



// alias and alias name=value: 	when no argument is provided, lists all aliases; when provided an
//			    	argument in the format key=value, sets an alias with the given
//				name and value. You do not need to handle the case when name
//				is a builtin function.
int Shell::com_alias(vector<string>& argv) {
	

	if (argv.size() == 1){
		// Print out alias map
		if (aliases.empty()){						// If no aliases, 
			cout << "No aliases" << endl;				//	then print there's no aliases
		} else {				
			for(map<string, string>::iterator itr = aliases.begin(); itr != aliases.end(); itr++){
			cout << itr->first << "=" << itr->second << endl;	// Else print out map
			}
		}
	} else if (argv.size() == 2){
		// Set the alias
		string alForm = argv[1];					// Create strings from input argument
		size_t eqPos = alForm.find("=");				// Find equal sign
		if (eqPos == string::npos){					// If no equal sign, return error.
			cout << "Wrong argument format, should be key=value" << endl;
			return EXIT_FAILURE;
		}
		string alKey = alForm.substr(0, eqPos);				// Create key string
		string alVal = alForm.substr(eqPos + 1);			// Create value stirng
		pair< map<string, string>::iterator,bool> ret;			// ret used for error checking
		ret = aliases.insert(pair<string, string>(alKey, alVal));	// Try to insert the key=value pair
		if (ret.second == false) {					// If key already exists, print error
			cout << "key " << alKey << " is already linked to value " << ret.first->second << endl;
			cout << "please delete orginanl  alias record to save this one" << endl;
		}		
	} else {					// Return error if one or more arguments given
		cout << "Too many arguments, I'm not that intelligent!!!!" << endl;
	 	return EXIT_FAILURE;			
	}
	return 0;								// Exit function
}

//unalias <name> and unalias -a: when provided with a -a argument, unsets all aliases;
//				other-wise, unsets the specified alias if set. 
//				You should not allow both -a and name to be passed at the
//				same time, as this doesn’t make sense.
int Shell::com_unalias(vector<string>& argv) {
		
 		
	if (argv.size() == 1) {
		// Print error, too few arguments
		cout << "No argument give, please try again" << endl;
		return EXIT_FAILURE;
	} else if (argv.size() == 2) {
		// Determing what argument wants and do it
		string inArg = argv[1];
		bool abandonShip;
		if (inArg == "-a") {				// Determine if user wants all alias deleted
			abandonShip = true;
		} else {
			abandonShip = false;
		}
		if (abandonShip == true) {			// Delete all alias
			aliases.clear();
		} else {					// Delete the passed in alias
			aliases.erase(inArg);
		}
	} else {
		// Print error, too man arguments give	
		cout << "Too many arguments, I'm not that intelligent!!!!" << endl;
 		return EXIT_FAILURE;
	}
	return 0;						// Exit function
}


// echo [arg1 arg2 ...  argN]: outputs each of the provided arguments, separated by spaces.
int Shell::com_echo(vector<string>& argv) {
	if (argv.size() == 1) {				// If no arguments, print a blank line
		cout << endl;
	} else {					// Else, if arguments print them out
		 for (unsigned int i = 1; i < argv.size(); i++) {
			cout << argv[i] << " ";
		}
 		 cout << endl; 
	}
	return 0;
}

// history: 	outputs all previously entered commands, along with their corresponding 1-based indexes
//		in the history. Only non-empty commands should be added to the history list.
int Shell::com_history(vector<string>& argv) {

	if (argv.size() == 1){ 	
		HIST_ENTRY** hist_entrys = history_list();		// Get history
		
		for (int i=0; hist_entrys[i]; i++) {			// Print historyq
			cout << " " <<  i+1 << " " << hist_entrys[i]->line << endl;
		}
	} else {
		// Print error, too man arguments give	
		cout << "Too many arguments, I'm not that intelligent!!!!" << endl;
 		return EXIT_FAILURE;
	}

  	return 0;
}


int Shell::com_exit(vector<string>& argv) {
	 exit(0);			// Exit the Shell
					// Might need to add more code for error checking and/or 
					// support differemt output codes.
}
