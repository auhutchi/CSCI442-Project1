/**
 * This file contains implementations of the functions that provide
 * tab-completion for the shell.
 *
 * You will need to finish the implementations of the completion functions,
 * though you're spared from implementing the high-level readline callbacks with
 * their weird static variables...
 */

#include "shell.h"
#include <cstdlib>
#include <iostream>
#include <readline/readline.h>
#include <readline/history.h>
#include <unistd.h>
#include <string>
#include <dirent.h>
#include <errno.h>

using namespace std;


void Shell::get_env_completions(const char* text, vector<string>& matches) {
 
	char **envLookUps = environ;				// Get enviroment list
	int envCount = 0;			
	string textStr = text;					// Turn text into a string
	if (textStr.substr(0, 1) == "$"){			// Deterimine if ti starts with $
		textStr = textStr.substr(1);
		
		// Test Enviroment vars
		while (envLookUps[envCount] != NULL) {		//Search through enviroment list
			string envVal = envLookUps[envCount];
			string envCut = envVal.substr(0,textStr.size()); 	//Get rid of the value 
			if (textStr == envCut) {		// If match, push to matches
				size_t eqPos = envVal.find('=');
				envCut = envVal.substr(0,eqPos);
				envCut = "$" + envCut;
				matches.push_back(envCut);
			}
			envCount++;
		}
		
		// Test local Vars
		string key;
		for(map<string, string>::iterator itr = localvars.begin(); itr != localvars.end(); itr++){
			key = itr->first;
			key = key.substr(0,textStr.size());
			if (key == textStr) {			// If text matches local var, push onto matches
				matches.push_back("$" + itr->first);
			}
		}		
	}
}


void Shell::get_command_completions(const char* text, vector<string>& matches) {
  
	string textStr = text;					// Turn text into a string
	string key;
	
	// Test aliases
	for(map<string, string>::iterator itr = aliases.begin(); itr != aliases.end(); itr++){
		key = itr->first;
		key = key.substr(0,textStr.size());
		if (key == textStr) {				// If an alias matches, push it onto matches
			matches.push_back(itr->first);
		}
	}
	
	// Test builtins
	for(map<string, builtin_t>::iterator itr = builtins.begin(); itr != builtins.end(); itr++){
		key = itr->first;
		key = key.substr(0,textStr.size());
		if (key == textStr) {				// If an builtin matches, push it onto matches
			matches.push_back(itr->first);
		}
	}

	// Test PATH
	char* getPath = getenv ("PATH");
	if (getPath != NULL){
		string pathStr = getPath;
		vector<string> directoryNames;
		size_t colPos;
		while(!pathStr.empty()){
			colPos = pathStr.find(":");
			if (colPos != string::npos){
				string directoryName = pathStr.substr(0, colPos);
				pathStr.erase(0, colPos + 1);
				directoryNames.push_back(directoryName);
			} else {
				directoryNames.push_back(pathStr);
				pathStr.clear();
			}
		}
		for (unsigned int i=0; i < directoryNames.size()-1; i++){
			DIR *directory;						// Create a directory stream
			struct dirent *dirRecords;				// Contains file names
			int errorSave;						// Used for error checking

			directory = opendir(directoryNames[i].c_str());		// Open a directory
			if (directory == NULL) {				// Check for errors
				cout << directoryNames[i] << endl;
				perror("Error opening directory ");
				exit(EXIT_FAILURE);
			}
		
			errorSave = errno;
			while ((dirRecords = readdir(directory)) != NULL) {
				key = dirRecords->d_name;
				key = key.substr(0, textStr.size());
				if (key == textStr){
					matches.push_back(dirRecords->d_name);
				}
			}
			closedir(directory);
			
			if (errorSave != errno) {
				perror("Error Reading Directory ");
				exit(EXIT_FAILURE);
			}
			
		}
	}	
}


char** Shell::word_completion(const char* text, int start, int end) {
  char** matches = NULL;

  if (text[0] == '$') {
    matches = rl_completion_matches(text, env_completion_generator);
  } else if (start == 0) {
    matches = rl_completion_matches(text, command_completion_generator);
  } else {
    // We get directory matches for free (thanks, readline!).
  }

  return matches;
}


char* Shell::env_completion_generator(const char* text, int state) {
  // A list of all the matches.
  // Must be static because this function is called repeatedly.
  static vector<string> matches;

  // If this is the first time called, construct the matches list with
  // all possible matches.
  if (state == 0) {
    getInstance().get_env_completions(text, matches);
  }

  // Return a single match (one for each time the function is called).
  return pop_match(matches);
}


char* Shell::command_completion_generator(const char* text, int state) {
  // A list of all the matches.
  // Must be static because this function is called repeatedly.
  static vector<string> matches;

  // If this is the first time called, construct the matches list with
  // all possible matches.
  if (state == 0) {
    getInstance().get_command_completions(text, matches);
  }

  // Return a single match (one for each time the function is called).
  return pop_match(matches);
}


char* Shell::pop_match(vector<string>& matches) {
  if (matches.size() > 0) {
    const char* match = matches.back().c_str();

    // Delete the last element.
    matches.pop_back();

    // We need to return a copy, because readline deallocates when done.
    char* copy = (char*) malloc(strlen(match) + 1);
    strcpy(copy, match);

    return copy;
  }

  // No more matches.
  return NULL;
}
